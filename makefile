all: slacks

slacks: main.o graph.o
	g++ main.o graph.o -o prog
        
graph.o: graph.cpp graph.h
	g++ -c graph.cpp -o graph.o
        
main.o: main.cpp
	g++ -c main.cpp 
clean:
	rm -f *.o
	rm -f prog
