#include "graph.h"

Graph::Graph(const char* nodes, const char* nets, const char* aux){
    ifstream nodes_stream(nodes), nets_stream(nets), aux_stream(aux);
    if (!nodes_stream)
        throw "problem with nodes file";
    if (!nets_stream)
        throw "problem with nets file";
    if (!aux_stream)
        throw "problem with aux file";
    double tmp_delay;
    vertex tmp_vertex;
    while (nodes_stream>>tmp_delay){
        tmp_vertex.delay=tmp_delay;
        tmp_vertex.aat_status=false;
        tmp_vertex.rat_status=false;
        vertices_.push_back(tmp_vertex);
    }
    nodes_stream.close();
    int v1,v2;
    edge tmp_edge;
    while (nets_stream>>v1>>v2>>tmp_delay){
        tmp_edge.num_vertex_from=v1;
        tmp_edge.num_vertex_to=v2;
        tmp_edge.delay=tmp_delay;
        edges_.push_back(tmp_edge);
        vertices_[v1].output_edges.push_back(edges_.size()-1);
        vertices_[v2].input_edges.push_back(edges_.size()-1);
    }
    nets_stream.close();
    double tmp_rat;
    while (aux_stream>>v1>>tmp_rat)
        vertices_[v1].rat=tmp_rat;
    aux_stream.close();
    find_all_aat();
    find_all_rat();
    find_all_slack();
}

double Graph::find_aat(int vert){
    if (vertices_[vert].input_edges.empty()){
        vertices_[vert].aat=vertices_[vert].delay;
        vertices_[vert].aat_status=true;
    }
    if (vertices_[vert].aat_status)
        return vertices_[vert].aat;
    double max_previous_delay=0, cure_previous_delay, previous_v_num;
    for (int i=0; i<(int)vertices_[vert].input_edges.size(); i++){
        previous_v_num=edges_[vertices_[vert].input_edges[i]].num_vertex_from;
        if ((cure_previous_delay=(edges_[vertices_[vert].input_edges[i]].delay+
                                find_aat(previous_v_num)))>max_previous_delay){
            max_previous_delay=cure_previous_delay;
        }
    }
    vertices_[vert].aat=max_previous_delay+vertices_[vert].delay;
    vertices_[vert].aat_status=true;
    return vertices_[vert].aat;
}

void Graph::find_all_aat(){
    for (int i=0; i<(int)vertices_.size(); i++){
        if (!vertices_[i].aat_status)
            find_aat(i); 
    }
}

double Graph::find_rat(int vert){
    if (vertices_[vert].output_edges.empty())
        vertices_[vert].rat_status=true;
    if (vertices_[vert].rat_status)
        return vertices_[vert].rat;
    double cure_next_request, next_v_num, min_next_request;
    next_v_num=edges_[vertices_[vert].output_edges[0]].num_vertex_to;
    min_next_request=-(edges_[vertices_[vert].output_edges[0]].delay
                                               -find_rat(next_v_num));
    for (int i=1; i<(int)vertices_[vert].output_edges.size(); i++){
        next_v_num=edges_[vertices_[vert].output_edges[i]].num_vertex_to;
        if ((cure_next_request=-(edges_[vertices_[vert].output_edges[i]].delay-
                                find_rat(next_v_num)))<min_next_request){
            min_next_request=cure_next_request;
        }
    }
    vertices_[vert].rat=min_next_request-vertices_[vert].delay;
    vertices_[vert].rat_status=true;
    return vertices_[vert].rat;
}

void Graph::find_all_rat(){
    for (int i=0; i<(int)vertices_.size(); i++)
        if (!vertices_[i].rat_status)
            find_rat(i); 
}

void Graph::find_all_slack(){
    for (int i=0; i<(int)vertices_.size(); i++)
        vertices_[i].slack=vertices_[i].rat-vertices_[i].aat;
}

void Graph::make_result_files(const char* slacks, const char* result){
    ofstream slacks_stream(slacks), result_stream(result);
    if (!slacks_stream)
        throw "problem with slacks file";
    if (!result_stream)
        throw "problem with result file";
    bool result_flag=false;
    for (int i=0; i<(int)vertices_.size(); i++){
        slacks_stream<<vertices_[i].slack<<endl;
        if ((vertices_[i].slack<0)&&(!result_flag)){
            result_flag=true; 
            result_stream<<1<<endl;
        }
        if ((vertices_[i].slack<0)&&(result_flag))
            result_stream<<i<<" ";
    }
    if (!result_flag)
        result_stream<<0;
    result_stream<<endl;
    slacks_stream.close();
    result_stream.close();
}
