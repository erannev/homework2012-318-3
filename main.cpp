#include "graph.h"

int main(){
    try{
        Graph graph(".nodes",".nets",".aux");
        graph.make_result_files(".slacks",".result");
    }
    catch (const char* message){
        cout<<message<<endl;
        return 1;
    }  
    return 0;
}
