#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class Graph{
public:
    Graph(const char* nodes, const char* nets, const char* aux);
    void make_result_files(const char* slacks, const char* result);
private:
    void find_all_aat();
    void find_all_rat();
    void find_all_slack();
    double find_aat(int vert);
    double find_rat(int vert);
    struct edge{
        double delay;
        int num_vertex_from;
        int num_vertex_to;
    };
    struct vertex{
        double delay;
        double aat;
        double rat;
        double slack;
        bool aat_status;
        bool rat_status;
        vector <int> input_edges;
        vector <int> output_edges;
    };
    vector <vertex> vertices_;
    vector <edge> edges_;
};

#endif   //GRAPH_H
