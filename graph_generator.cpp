#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
/*Создает 3 файла .nodes .nets .aux в заданном формате
 -количество вершин задается через командную строку
 первый параметр - количество вершин, второй - ребер.
 -граф создается ациклический
 -в графе минимум 1 вход (первая вершина).
 -в графе минимум 1 выход (последняя вершина).
 -в случае единственной вершиниы они совпадают.
 -контролирует возможность создания ациклического графа с таким
 количеством вершин и ребер.
 -задержка ребер и вершин 0.0-9.9, RAT выходов 0.0-399.9
*/

using namespace std;

bool cycle_test(int i, int j, double** matrix, int matrix_size){
    if (matrix[j][i]!=0)
        return false;
    for (int k=0; k<matrix_size; k++)
        if (matrix[j][k]!=0)
            if (!cycle_test(i, k, matrix, matrix_size))
                return false;
    return true;
}

int main(int argc, char** argv){
    if (argc<3){
        cout<<"error : number of arguments < 3"<<endl;
        exit(1);
    }
    srand(time(NULL));
    int graph_size=atoi(argv[1]), number_of_edges=atoi(argv[2]);
    double vertices[graph_size];
    int sum_from_1_to_graph_size=0;
    if (graph_size<=0){
        cout<<"error : number of vertices <= 0"<<endl;
        exit(1);
    }
    for (int i=0; i<graph_size; i++)
        sum_from_1_to_graph_size+=graph_size-1-i;
    if (sum_from_1_to_graph_size<number_of_edges){
        cout<<"error : to many edges for this number of vertices"<<endl;
        exit(1);
    }
    double** adjacency_matrix=new double* [graph_size]; 
    for (int i=0; i<graph_size; i++){
        vertices[i]=((double)(rand()%100))/10;
        adjacency_matrix[i]=new double [graph_size];
        for (int j=0; j<graph_size; j++)
            adjacency_matrix[i][j]=0;
    }
    int current_edges_number=0, i, j;
    while (current_edges_number<number_of_edges){
        i=rand()%(graph_size-1);
        j=rand()%(graph_size-1)+1;
        if ((i==j) || (adjacency_matrix[i][j]!=0))
            continue;
        if (!cycle_test(i,j,adjacency_matrix,graph_size))
            continue;
        adjacency_matrix[i][j]=((double)(rand()%100))/10;
        current_edges_number++;        
    }
    ofstream nodes_stream(".nodes"), nets_stream(".nets"), aux_stream(".aux");
    if ((!nodes_stream)||(!nets_stream)||(!aux_stream)){
        cout<<"problem with file(s)"<<endl;
        exit(1);
    }
    double rat;
    bool flag=true;
    for (int i=0; i<graph_size; i++){
        nodes_stream<<vertices[i]<<endl;
        for (int j=0; j<graph_size; j++)
            if (adjacency_matrix[i][j]!=0){
                nets_stream<<i<<" "<<j<<" "<<adjacency_matrix[i][j]<<endl;
                flag=false;
            }
        if (flag){
            rat=((double)(rand()%4000))/10;
            aux_stream<<i<<" "<<rat<<endl;
        }
        flag=true;
    }
    nodes_stream.close();
    nets_stream.close();
    aux_stream.close();
    cout<<"succes!"<<endl;
    return 0;
}